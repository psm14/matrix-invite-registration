use clap::Clap;

#[derive(Clap, Clone)]
#[clap(version = env!("CARGO_PKG_VERSION"), author = env!("CARGO_PKG_AUTHORS"))]
pub struct Opts {
    /// Auth secret to create tokens
    #[clap(short, long, required = true)]
    pub auth_secret: String,
    /// Synapse server hostname
    #[clap(short, long, required = true)]
    pub synapse_hostname: String,
    /// Synapse registration secret
    #[clap(short, long, required = true)]
    pub registration_shared_secret: String,
    /// Port to listen on
    #[clap(short, long, default_value = "3000")]
    pub port: u16,
    /// Address to bind
    #[clap(short, long, default_value = "127.0.0.1")]
    pub host: String,
    /// Database file
    #[clap(short, long, default_value = "invites.db")]
    pub database: String,
    /// Use in-memory db
    #[clap(short, long)]
    pub memory_db: bool,
    /// API root
    #[clap(long, default_value = "_token")]
    pub api_root: String,
    /// Log requests
    #[clap(short, long)]
    pub verbose: bool,
    /// Static asset dir
    #[clap(long, default_value = "static")]
    pub static_directory: String,
}
