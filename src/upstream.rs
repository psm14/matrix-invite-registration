use serde::{Deserialize, Serialize};

use crate::args::Opts;
use crate::hmac::compute_hmac;

pub struct UpstreamApi<'a> {
    hostname: &'a str,
    registration_shared_secret: &'a str,
}

impl<'a> UpstreamApi<'a> {
    pub fn from_opts(opts: &'a Opts) -> Self {
        UpstreamApi {
            hostname: opts.synapse_hostname.as_str(),
            registration_shared_secret: opts.registration_shared_secret.as_str(),
        }
    }

    pub async fn get_nonce(self: &Self) -> surf::Result<String> {
        let uri = format!["https://{}/_matrix/client/r0/admin/register", self.hostname];
        let NonceResponse { nonce } = surf::get(uri).recv_json().await?;
        Ok(nonce)
    }

    pub async fn register(
        self: &Self,
        nonce: &str,
        username: &str,
        password: &str,
        admin: bool,
    ) -> surf::Result<RegisterResponse> {
        let mac = compute_hmac(
            self.registration_shared_secret,
            nonce,
            username,
            password,
            admin,
        );
        let body = RegisterBody {
            nonce,
            username,
            password,
            admin,
            mac: &mac,
        };

        let uri = format!["https://{}/_matrix/client/r0/admin/register", self.hostname];
        let mut result = surf::post(uri).body(surf::Body::from_json(&body)?).await?;
        let body: RegisterResponse = result.body_json().await?;
        Ok(body)
    }

    pub async fn join_room(self: &Self, room_id: &str, access_token: &str) -> surf::Result<()> {
        let room_id = urlencoding::encode(&room_id);
        let access_token = urlencoding::encode(&access_token);
        let uri = format![
            "https://{}/_matrix/client/r0/rooms/{}/join?access_token={}",
            self.hostname, room_id, access_token
        ];
        let body = EmptyBody {};

        surf::post(uri).body(surf::Body::from_json(&body)?).await?;

        Ok(())
    }

    pub async fn logout(self: &Self, access_token: &str) -> surf::Result<()> {
        let access_token = urlencoding::encode(&access_token);
        let uri = format![
            "https://{}/_matrix/client/r0/logout?access_token={}",
            self.hostname, access_token
        ];
        let body = EmptyBody {};

        surf::post(uri).body(surf::Body::from_json(&body)?).await?;

        Ok(())
    }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct NonceResponse {
    pub nonce: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct RegisterBody<'a> {
    nonce: &'a str,
    username: &'a str,
    password: &'a str,
    admin: bool,
    mac: &'a str,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RegisterResponse {
    pub access_token: String,
    pub device_id: String,
    pub home_server: String,
    pub user_id: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct EmptyBody {}
