use names::{Generator, Name};
use serde::{Deserialize, Serialize};
use std::{fs, path::Path};
use tide::{http::mime, log, Error, Request, Response, Server};

mod db;
use db::InviteDatabase;

mod hmac;

mod args;
use args::Opts;
use clap::Clap;

mod upstream;
use upstream::UpstreamApi;

mod extensions;
use extensions::*;

#[derive(Clone)]
struct AppState {
    db: InviteDatabase,
    opts: Opts,
}

#[async_std::main]
async fn main() -> tide::Result<()> {
    let opts: Opts = Opts::parse();

    let db = if opts.memory_db {
        InviteDatabase::open_in_memory()?
    } else {
        InviteDatabase::open(&opts.database)?
    };

    let state = AppState {
        db,
        opts,
    };

    let mut app = Server::with_state(state.clone());
    app.at(&format!["/{}/create", state.opts.api_root])
        .post(create_token);
    app.at(&format!["/{}/redeem", state.opts.api_root])
        .post(redeem_token);

    app.at(&format!["/{}/register", state.opts.api_root])
        .get(serve_registration_page);
    app.at(&format!["/{}", state.opts.api_root])
        .serve_dir(state.opts.static_directory)?;

    let address = format!["{}:{}", state.opts.host, state.opts.port];

    if state.opts.verbose {
        log::start();
    } else {
        // Tide prints an equivalent message if logging is enabled, so only
        // print it explicitly if verbose is off
        println!["Listening on {}", address];
    }

    app.listen(address).await?;
    Ok(())
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct CreateTokenBody {
    #[serde(default = "default_admin")]
    admin: bool,
    #[serde(default = "default_rooms")]
    rooms: Vec<String>,
}

fn default_admin() -> bool {
    false
}

fn default_rooms() -> Vec<String> {
    vec![]
}

#[derive(Debug, Serialize, Deserialize)]
struct CreateTokenResponse {
    token: String,
    uri: String,
}

async fn create_token(mut req: Request<AppState>) -> tide::Result {
    let secret = req.state().opts.auth_secret.as_str();
    let match_value = format!["Bearer {}", secret];
    let auth_value = req
        .header("Authorization")
        .or_error(401, "Unauthorized")?
        .as_str();
    if auth_value != match_value {
        return Err(Error::from_str(401, "Unauthorized"));
    }

    let CreateTokenBody { admin, rooms } = try_parse_body(&mut req).await?;

    let mut generator = Generator::new(names::ADJECTIVES, names::NOUNS, Name::Numbered);
    let token = generator.next().or_error(500, "Error generating token")?;

    let conn = &req.state().db;
    conn.add_invite(&token, admin, &rooms.join(","))
        .await
        .or_error(500, "Database error")?;

    let opts = &req.state().opts;
    let uri = format![
        "https://{}/{}/register#{}",
        opts.synapse_hostname, opts.api_root, token
    ];

    let response = CreateTokenResponse { token, uri };
    Ok(Response::builder(200)
        .body(tide::Body::from_json(&response)?)
        .build())
}

#[derive(Debug, Serialize, Deserialize, Clone)]
struct RedeemTokenBody {
    username: String,
    password: String,
    token: String,
}

async fn redeem_token(mut req: Request<AppState>) -> tide::Result {
    let RedeemTokenBody {
        username,
        password,
        token,
    } = try_parse_body(&mut req).await?;

    let conn = &req.state().db;
    let token_info = conn
        .get_token(&token)
        .await
        .or_error(500, "Database error")?
        .or_error(401, "Invalid token")?;

    if !token_info.active {
        return Err(Error::from_str(401, "Invalid token"));
    }

    let api = UpstreamApi::from_opts(&req.state().opts);

    let nonce = api
        .get_nonce()
        .await
        .or_error(502, "Upstream server error")?;

    let register_response = api
        .register(&nonce, &username, &password, token_info.admin)
        .await
        .or_error(502, "Upstream server error")?;

    conn.expire_token(&token)
        .await
        .or_error(500, "Database error")?;

    for room_id in token_info.rooms.split(",") {
        if room_id.len() > 0 {
            // Ignoring result because if this fails then it's best to just keep
            // going since the account is already registered.
            let _result = api
                .join_room(room_id, &register_response.access_token)
                .await;
        }
    }

    // If this fails then all that happens is an extra session will be visible
    // on the account screen, and it can just be invalidated there.
    let _result = api.logout(&register_response.access_token).await;

    let redirect_uri = format!["https://{}/", req.state().opts.synapse_hostname];
    Ok(tide::Redirect::new(redirect_uri).into())
}

async fn serve_registration_page(req: Request<AppState>) -> tide::Result {
    let opts = &req.state().opts;
    let registration_html_path = Path::new(&opts.static_directory).join("registration.html");
    let body = fs::read_to_string(registration_html_path)?;
    Ok(Response::builder(200)
        .content_type(mime::HTML)
        .body(body)
        .build())
}

// Tried to do this as an extension method (is that the correct Rust-y term?)
// But it appears to require using dynamic dispatch since async and impl Trait 
// return types can't be used in trait functions.
async fn try_parse_body<T, S>(req: &mut Request<S>) -> tide::Result<T>
where
    for<'de> T: Deserialize<'de>,
{
    let is_json = req
        .header("Content-Type")
        .map(|h| h == "application/json")
        .unwrap_or(false);

    if is_json {
        req.body_json().await.or_error(400, "Bad request body")
    } else {
        req.body_form().await.or_error(400, "Bad request body")
    }
}
