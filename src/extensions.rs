use std::{
    convert::TryInto,
    fmt::{Debug, Display},
};
use tide::{Error, StatusCode};

pub trait RequestExtensions<A> {
    fn or_error<S, M>(self: Self, status: S, msg: M) -> Result<A, Error>
    where
        S: TryInto<StatusCode>,
        S::Error: Debug,
        M: Display + Debug + Send + Sync + 'static;
}

impl<A> RequestExtensions<A> for Option<A> {
    fn or_error<S, M>(self: Self, status: S, msg: M) -> Result<A, Error>
    where
        S: TryInto<StatusCode>,
        S::Error: Debug,
        M: Display + Debug + Send + Sync + 'static,
    {
        self.ok_or(Error::from_str(status, msg))
    }
}

impl<A, B> RequestExtensions<A> for Result<A, B> {
    fn or_error<S, M>(self: Self, status: S, msg: M) -> Result<A, Error>
    where
        S: TryInto<StatusCode>,
        S::Error: Debug,
        M: Display + Debug + Send + Sync + 'static,
    {
        self.map_err(|_e| Error::from_str(status, msg))
    }
}
