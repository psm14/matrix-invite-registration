use async_std::sync::{Arc, Mutex};
use rusqlite::{params, Connection, Result};

#[derive(Clone)]
pub struct InviteDatabase {
    connection: Arc<Mutex<Connection>>,
}

pub struct GetTokenResult {
    pub active: bool,
    pub admin: bool,
    pub rooms: String,
}

impl InviteDatabase {
    pub fn open(filename: &str) -> Result<Self> {
        let conn = Connection::open(filename)?;
        Self::initialize_db(conn)
    }

    pub fn open_in_memory() -> Result<Self> {
        let conn = Connection::open_in_memory()?;
        Self::initialize_db(conn)
    }

    fn initialize_db(conn: Connection) -> Result<Self> {
        conn.execute(
            "CREATE TABLE IF NOT EXISTS invites (
                token TEXT NOT NULL,
                active INTEGER NOT NULL,
                admin INTEGER NOT NULL,
                rooms TEXT NOT NULL,
                PRIMARY KEY(token)
            )",
            params![],
        )?;

        Ok(InviteDatabase {
            connection: Arc::new(Mutex::new(conn)),
        })
    }

    pub async fn add_invite(self: &Self, token: &str, admin: bool, rooms: &str) -> Result<()> {
        let admin = if admin { 1 } else { 0 };

        self.connection.lock().await.execute(
            "INSERT INTO invites
                (token, active, admin, rooms)
             VALUES (?1, 1, ?2, ?3)",
            params![token, admin, rooms],
        )?;

        Ok(())
    }

    pub async fn get_token(self: &Self, token: &str) -> Result<Option<GetTokenResult>> {
        let conn = self.connection.lock().await;

        let mut stmt = conn.prepare(
            "SELECT active, admin, rooms
             FROM invites
             WHERE token = ?1
             LIMIT 1",
        )?;
        let mut iter = stmt.query_map(params![token], |row| {
            Ok(GetTokenResult {
                active: row.get(0)?,
                admin: row.get(1)?,
                rooms: row.get(2)?,
            })
        })?;

        if let Some(row) = iter.next() {
            Ok(Some(row?))
        } else {
            Ok(None)
        }
    }

    pub async fn expire_token(self: &Self, token: &str) -> Result<bool> {
        let conn = self.connection.lock().await;
        let num_rows = conn.execute(
            "UPDATE invites
             SET active = 0
             WHERE token = ?1 AND active = 1",
            params![token],
        )?;
        Ok(num_rows > 0)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[async_std::test]
    async fn smoke_test() -> rusqlite::Result<()> {
        let db = InviteDatabase::open_in_memory()?;
        db.add_invite("foobar", false, "").await?;
        let result = db.get_token("foobar").await?;
        assert![result.is_some()];
        let result = result.unwrap();
        assert_eq![result.active, true];
        assert_eq![result.admin, false];
        assert_eq![result.rooms, ""];
        db.expire_token("foobar").await?;
        let result = db.get_token("foobar").await?;
        assert![result.is_some()];
        let result = result.unwrap();
        assert_eq![result.active, false];

        Ok(())
    }

    #[async_std::test]
    async fn dont_allow_double_redeem() -> rusqlite::Result<()> {
        let db = InviteDatabase::open_in_memory()?;
        db.add_invite("foobar", false, "").await?;
        assert_eq![db.expire_token("foobar").await?, true];
        assert_eq![db.expire_token("foobar").await?, false];

        Ok(())
    }
}
