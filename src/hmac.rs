use hmac::{Hmac, Mac, NewMac};
use sha1::Sha1;

pub fn compute_hmac(
    shared_secret: &str,
    nonce: &str,
    username: &str,
    password: &str,
    admin: bool,
) -> String {
    let mut result =
        Hmac::<Sha1>::new_varkey(shared_secret.as_bytes()).expect("Invalid HMAC shared secret");
    result.update(nonce.as_bytes());
    result.update(b"\x00");
    result.update(username.as_bytes());
    result.update(b"\x00");
    result.update(password.as_bytes());
    result.update(b"\x00");
    result.update(if admin { b"admin" } else { b"notadmin" });

    let digest = result.finalize();
    hex::encode(digest.into_bytes())
}
